import numpy as np
import json
from os import walk
from tqdm import tqdm


def xyxy_to_xywh(xyxy):
    if np.array(xyxy).ndim > 1 or len(xyxy) > 4:
        raise ValueError('xyxy format: [x1, y1, x2, y2]')
    x_temp = (xyxy[0] + xyxy[2]) / 2
    y_temp = (xyxy[1] + xyxy[3]) / 2
    w_temp = abs(xyxy[0] - xyxy[2])
    h_temp = abs(xyxy[1] - xyxy[3])
    width = 2042
    length = 1076
    return np.array([float(x_temp) / width, float(y_temp) / length, float(w_temp) / width, float(h_temp) / length])


def has_point(detectie, points):
    for point in points:
        if detectie["points"]["exterior"][0][0] <= point["points"]["exterior"][0][0]:
            if detectie["points"]["exterior"][0][1] <= point["points"]["exterior"][0][1]:
                if detectie["points"]["exterior"][1][0] >= point["points"]["exterior"][0][0]:
                    if detectie["points"]["exterior"][1][1] >= point["points"]["exterior"][0][1]:
                        return True


def get_point(detectie, points):
    for point in points:
        if detectie["points"]["exterior"][0][0] <= point["points"]["exterior"][0][0]:
            if detectie["points"]["exterior"][0][1] <= point["points"]["exterior"][0][1]:
                if detectie["points"]["exterior"][1][0] >= point["points"]["exterior"][0][0]:
                    if detectie["points"]["exterior"][1][1] >= point["points"]["exterior"][0][1]:
                        return point


def main():
    meta = json.load(open("meta.json"))
    # This does assume there are only 2 classes (annotation, point)
    for count, i in enumerate(meta["classes"]):
        if count == 0:
            class_one = i["title"]
        else:
            sec = i["title"]
    class_to_id = {class_one: 0, sec: 1}

    for (dirpath, dirnames, filenames) in walk("input"):
        for filename in tqdm(filenames):
            og_ann = f"input/{filename}"
            data = json.load(open(og_ann))
            yolo_list = []
            point_list = []
            yolo_output_list = []
            point_output_list = []
            for i in data["objects"]:
                if i["geometryType"] == "rectangle":
                    yolo_list += [i]
                elif i["geometryType"] == "point":
                    point_list += [i]
            for ann in yolo_list:
                xyxy = [ann["points"]["exterior"][0][0], ann["points"]["exterior"][0][1],
                        ann["points"]["exterior"][1][0],
                        ann["points"]["exterior"][1][1]]
                x, y, w, h = xyxy_to_xywh(xyxy)
                yolo_output_list += [
                    f"{class_to_id[ann['classTitle']]} {'%.6f' % x} {'%.6f' % y} {'%.6f' % w} {'%.6f' % h}"]
                if has_point(ann, point_list):
                    p = get_point(ann, point_list)
                    point_output_list += [
                        f"{class_to_id[p['classTitle']]} {p['points']['exterior'][0]}"]
                else:
                    point_output_list += ["-"]
            with open(f"output/{filename.split('.')[0]}.txt", "w") as file:
                for i in yolo_output_list:
                    file.write(f"{i}\n")
            with open(f"output/{filename.split('.')[0]}_points.txt", "w") as file:
                for i in point_output_list:
                    file.write(f"{i}\n")
        break


if __name__ == "__main__":
    main()
